module ChronalCoords
export load_data, get_borders, get_all_points, get_distances, part_one, part_two

import CSV
using Distances

function load_data(file)
    res = CSV.read(file, delim=",", header=false)
    return convert(Matrix, res)
end

function get_borders(data)
    return minimum(data[:, 1])-1, minimum(data[:, 2])-1, maximum(data[:, 1])+1, maximum(data[:, 2])+1
end

function get_all_points(data)
    lower_x, lower_y, upper_x, upper_y = get_borders(data)
    return hcat([[x,y] for (x, y) in Iterators.product(lower_x:upper_x, lower_y:upper_y)]...)
end

# https://github.com/JuliaStats/Distances.jl
function get_distances(data)
    points = get_all_points(data)
    return pairwise(Cityblock(), transpose(data), points)
end

function part_one(data)
    distances = get_distances(data)
    min_dist = minimum(distances, dims=1)
    argmin_dist = [i[1] for i in argmin(distances, dims=1)]
    nomansland = sum(distances .== min_dist, dims=1) .> 1
    argmin_dist[nomansland] .= 0
    lower_x, lower_y, upper_x, upper_y = get_borders(data)
    dim_x = upper_x - lower_x + 1
    dim_y = upper_y - lower_y + 1
    argmin_dist_mx = reshape(argmin_dist, dim_x, dim_y)
    infinite_regs = union(argmin_dist_mx[1, :],
                          argmin_dist_mx[:, 1],
                          argmin_dist_mx[end, :],
                          argmin_dist_mx[:, end])
    finite_regs = setdiff(1:size(data)[1], infinite_regs)
    surfaces = [sum(argmin_dist_mx .== i) for i in finite_regs]
    return maximum(surfaces)
end

function part_two(data, threshold)
    distances = get_distances(data)
    sum_distances = sum(distances, dims=1)
    return sum(sum_distances .< threshold)
end

end

