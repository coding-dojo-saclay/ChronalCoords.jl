using Test
using ChronalCoords

@testset "Chronal coordinates" begin
    FILE = "sample.txt"
    DATA = nothing
    @test (DATA = load_data(FILE)) == [1 1; 1 6; 8 3; 3 4; 5 5; 8 9]
    @test get_borders(DATA) == (0, 0, 9, 10)
    @test size(get_all_points(DATA)) == (2, 110)
    @test size(get_distances(DATA)) == (6, 110)
    @test part_one(DATA) == 17
    #@test part_one(load_data("data.txt")) == ? [data.txt must contain your puzzle input]
    #@test part_two(load_data("data.txt"), 10000) == ? [data.txt must contain your puzzle input]
end
