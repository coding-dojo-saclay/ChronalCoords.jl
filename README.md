# ChronalCoords.jl
This is a solution for the Day 6 puzzle of "Advent of Code 2018", available here: https://adventofcode.com/2018/day/6

Recently, another solution was proposed by François Févotte with a different approach: https://gitlab.inria.fr/coding-dojo-saclay/ChronalCoords2.jl/